nextRow current coin = [if snd x >= coin then current !! (snd x - coin) + fst x else fst x | x <- zip current [0,1..]]

main = do
    print $ nextRow [1, 0, 0, 0, 0, 0] 1
    print $ nextRow [1, 1, 1, 1, 1, 1] 2
    print $ nextRow [1, 1, 2, 2, 3, 3] 5


   0  1  2  3  4  5
0  1  0  0  0  0  0

1  1  

2  1

5  1
 
