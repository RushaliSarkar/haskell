import Data.Bits

reverseBits :: Int -> Int -> Int -> Int
reverseBits 0 pow reversed = reversed
reverseBits num pow reversed  = reverseBits num   
