import Data.List.Split
import Data.Maybe
import qualified Data.Map.Strict as Map

intTime = [0, 1..59]
stringTime = [if x < 10 then "0" ++ show x else show x | x <- intTime]
timeMap = Map.fromAscList $ zip intTime stringTime

splitTime :: [Char] -> [[Char]]
splitTime time = splitOn ":" time

convertToInt :: [[Char]] -> [Int]
convertToInt x | length x == 0 = []
convertToInt x | otherwise = [read (head x) :: Int] ++ convertToInt (tail x)

convertToString :: [Int] -> [Char]
convertToString time = fromJust (Map.lookup (head time) timeMap) ++ ":" ++ fromJust (Map.lookup (last time) timeMap)

addDelta :: [Char] -> [Char] -> [Char]
addDelta time1 time2 = convertToString ([((head (convertToInt (splitTime time1))) + (head (convertToInt (splitTime time2)))) `mod` 24] ++ [((last (convertToInt (splitTime time1))) + (last (convertToInt (splitTime time2)))) `mod` 60])

toString :: [Char] -> [Char]
toString time = convertToString $ convertToInt (splitTime time)
 
main = do
    print $ addDelta "12:01" "23:59"
    print $ toString "1:21"    
