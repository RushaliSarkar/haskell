isLeapYear :: Int -> Bool
isLeapYear year | year `mod` 4 /= 0 = False
isLeapYear year | year `mod` 100 == 0 = year `mod` 400 == 0
isLeapYear year = True


main = do

    print $ isLeapYear 2000
    print $ isLeapYear 1800
    print $ isLeapYear 1700
