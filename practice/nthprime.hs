isPrime :: Int -> Bool
isPrime n | n <= 1 = False
isPrime n | elem n [2, 3, 5, 7, 11] = True
isPrime n = sum ([each | each <- [2..n - 1], mod n each == 0]) == 0


nthPrime :: Int -> Int
nthPrime n | n <= 1 = 0
nthPrime n = last $ take n [each | each <- [1,2..], isPrime each]

main = do

    print $ nthPrime 4
    print $ nthPrime 10
    print $ nthPrime 7
    print $ nthPrime 5
