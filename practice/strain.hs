keep :: [Int] -> [Int]
keep x = [each | each <- x, even each]

discard :: [Int] -> [Int]
discard x = [each | each <- x, odd each]

strain :: [Int] -> [[Int]]
strain x = [keep x, discard x]

main = do

    print $ strain [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
