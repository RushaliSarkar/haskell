import Data.Char

splitIt :: [Char] -> [Int]
splitIt [] = []
splitIt x = [digitToInt each | each <- x, elem each "0123456789"]

doubleSecond :: [Int] -> [Int]
doubleSecond x = [if even index then (x !! index) * 2 else x !! index | index <- [0..length x - 1]]

doubleEvenIndeices :: [Int] -> [Int]
doubleEvenIndices x = zipWith (*) split

reduceDoubleDigits :: [Int] -> [Int]
reduceDoubleDigits x = [if each > 9 then each - 9 else each | each <- x]

isValid :: [Char] -> Bool
isValid creditCard | length creditCard /= 19 = False
isValid creditCard = mod (sum (reduceDoubleDigits (doubleSecond (splitIt creditCard)))) 10 == 0

main = do

    print $ isValid "4539 3195 0343 6467"    
    print $ isValid "8569 6195 0383 3437"
    print $ isValid "8273 1232 7352 0569"
    print $ isValid "7253 2262 5312 0539"
