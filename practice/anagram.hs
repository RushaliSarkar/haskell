letterToFrequency :: [Char] -> [(Char, Int)]
letterToFrequency word = [(each, count) | each <- ['a'..'z'], let count = length (filter (==each) word)]

isAnagram :: [Char] -> [Char] -> Bool
isAnagram word1 word2 = (letterToFrequency word1) == (letterToFrequency word2)

anagrams :: [Char] -> [[Char]] -> [[Char]]
anagrams masterWord words = [each | each <- words, isAnagram masterWord each]


main = do

        print $ anagrams "teenager" ["ate green", "green tea", "gene tear", "rage teen", "agree ten", "teen anger", "angry teen"]  
        print $ anagrams "imposter" ["more tips", "permit so", "prism toe", "permitted", "rare"]

