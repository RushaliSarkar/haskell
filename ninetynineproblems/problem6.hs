isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome x = reverse x == x

main = do
    print $ isPalindrome "Rushali"
    print $ isPalindrome "abcdcba"
