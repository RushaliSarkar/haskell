findLength :: [a] -> Int
findLength = length

main = do
    print $ findLength "Haskell"
    print $ findLength [1, 2, 3, 4, 5, 6, 7]
