findkth :: [a] -> Int -> a
findkth x k = x !! (k - 1)

main = do
    print $ findkth [1, 2, 3] 2
    print $ findkth "Haskell" 5
