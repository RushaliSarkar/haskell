lastbutone :: [a] -> a
lastbutone x = (reverse x) !! 1

main = do
    print $ lastbutone [1, 2, 3, 4, 5, 6, 7, 8]
