findReverse :: [a] -> [a]
findReverse = reverse

main = do
    print $ findReverse [1, 2, 3, 4, 5, 6, 7]
    print $ findReverse "haskell" 
