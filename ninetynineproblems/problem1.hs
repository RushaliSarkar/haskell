findLast :: [a] -> a
findLast [] = error "The list is empty"
findLast [x] = x
findLast (x:xs) = findLast xs 


main = do
    print $ findLast [1, 2, 3, 4, 5, 6, 7]
